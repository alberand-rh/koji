# Create a config in ~/.koji/config:
#
# [koji]
# server = https://brewhub.engineering.redhat.com/brewhub
# authtype = kerberos
# krbservice = brewhub
# topdir = /mnt/redhat/brewroot
# weburl = https://brewweb.engineering.redhat.com/brew
# topurl = http://download.devel.redhat.com/brewroot
# use_fast_upload = yes
# anon_retry = yes
# serverca = /etc/pki/tls/certs/ca-bundle.crt
# poll_interval = 11
{
  description = "Koji";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      koji-drv = pkgs.python311Packages.buildPythonApplication {
        name = "koji";

        src = ./.;

        propagatedBuildInputs = with pkgs.python311Packages; [
          six
          dateutil
          requests
          defusedxml
          requests-gssapi
        ];

        doCheck = false;
      };
    in {
      packages = {
        koji = koji-drv;
        default = koji-drv;
      };

      apps = rec {
        koji = flake-utils.lib.mkApp {
          drv = self.packages.${system}.koji;
        };
        default = koji;
      };
    });
}
